# MUSA Excel data importer


[![build status](https://git.jacekdominiak.com/ca/musa-excel-importer/badges/master/build.svg)](https://git.jacekdominiak.com/ca/musa-excel-importer/commits/master)

This is the repository for the MUSA Excel Data Importer. 

The main objective of MUSA, MUlti-cloud Secure Applications,  is to support the security-intelligent lifecycle management of distributed applications over heterogeneous cloud resources, through a security framework that includes:
* security-by-design mechanisms to allow application self-protection at runtime, and
* methods and tools for the integrated security assurance in both the engineering and operation of multi-cloud applications.

The MUSA framework leverages security-by-design, agile and DevOps approaches in multi-cloud applications, and enables the security-aware development and operation of multi-cloud applications. The framework will be composed of
* an IDE for creating the multi-cloud application taking into account its security requirements together with functional and business requirements,
* a set of security mechanisms embedded in the multi-cloud application components for self-protection,
* an automated deployment environment that, based on an intelligent decision support system, will allow for the dynamic distribution of the components according to security needs, and
* a security assurance platform in form of a SaaS that will support multi-cloud application runtime security control and transparency to increase user trust.

The project will demonstrate and evaluate the economic viability and practical usability of the MUSA framework in highly relevant industrial applications representative of multi-cloud application development potential in Europe.
The project is 36 month long. It was started in January 2015 and should have its results ready by the end of 2017.

For more information, visit [http://www.musa-project.eu/](http://www.musa-project.eu/)

## How to use this tool
```
USAGE: musa-excel-import.exe [--help] --excel-file <path> [--excel-worksheet <sheet>] --map-file <path> [--output-type <format>]
                             [--table-name <name>]

OPTIONS:

    --excel-file <path>   specify source Excel file (xls|xlsx)
    --excel-worksheet <sheet>
                          specify a worksheet name to process, if none given, the first one worksheet of the file is taken
    --map-file <path>     specify transformation map file in CSV format
    --output-type <format>
                          specify output type (default: Json) <json|csv|sql>
    --table-name <name>   specify final database tableName
    --help                display this list of options.
```

## Development information

**WARNING! Please work on each integration on a separate branch other than *master*.**

### Dependencies installation 
The overlaying application has been developed using [F#](http://fsharp.org/) programming language. Each of the subdevided solutions are stored in their subsequent directories. All the dependencies are managed using [Paket](https://fsprojects.github.io/Paket/) which is well established and accepted dependecy manager for .Net and mono projects. 

### Minimum requirements for development
#### On Windows
* .Net compilers or [Mono](http://www.mono-project.com/)

#### On any other operating system
* [Mono](http://www.mono-project.com/)

### Minimum requirements to run
#### On Windows
- none
#### On any other operating system
* [Mono](http://www.mono-project.com/)

### Build process
In order to build the tool, simply launch `build.sh` or `build.cmd` accordingly to the platform you are using. Once build process is completed, you can find the tool under `build` folder in the root of the solution. 

## License
**The MIT License (MIT)**
Copyright (c) MUSA, 2016

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.