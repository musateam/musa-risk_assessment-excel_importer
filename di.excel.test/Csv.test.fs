namespace DI.Excel.Test

open NUnit.Framework
open FsUnit
open ExcelPackageF
open FSharp.Data

[<TestFixture>]
type ``CSV data extraction`` () =

    let excelDataMap = @"../tools/testData/testDataMap.csv"
    
    [<Test>]
    member x.``Read the file`` () =
        excelDataMap 
        |> DI.Csv.readCsv 
        |> should be ofExactType<CsvFile>

    [<Test>]
    member x.``Get the headers`` () =
        let headers = excelDataMap 
                    |> DI.Csv.readCsv
                    |> DI.Csv.getColumnNames
    
        headers.[0] |> should equal "from"
       
    [<Test>]
    member x.``Get data w/o columns`` () =
        excelDataMap
        |> DI.Csv.readCsv
        |> DI.Csv.getRowsData
        |> Seq.length
        |> should equal 2

    [<Test>]
    member x.``Get exact data position`` () =
        let csvData = excelDataMap |> DI.Csv.readCsv
        DI.Csv.getExactColumnRow csvData "to" 2 
        |> should equal "Column A"