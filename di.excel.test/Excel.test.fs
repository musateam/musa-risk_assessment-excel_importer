namespace DI.Excel.Test

open NUnit.Framework
open FsUnit
open ExcelPackageF
open FSharp.Data


[<TestFixture>]
type ``Excel data extraction`` () =

    let excelPath = @"../tools/testData/testData.xlsx" 
    let excelData = DI.Excel.readExcel excelPath null
    let excelDataMap = @"../tools/testData/testDataMap.csv"

    [<Test>]
    member x.``Read the file`` () =
        excelData |> should be ofExactType<OfficeOpenXml.ExcelWorksheet>

    [<Test>]
    member x.``Get Column Names`` () = 
        DI.Excel.getColumnNames excelData 
        |> String.concat ", " 
        |> should equal "Column 1, Column2"

    [<Test>]
    member x.``Get Column Count`` () =
        DI.Excel.getColumnCount excelData
        |> should equal 2

    [<Test>]
    member x.``Get data w/o columns`` () =
        DI.Excel.getRowsData excelData
        |> Seq.length
        |> should equal 4

    [<Test>]
    member x.``Match column name`` () =
        let fromToPairs = DI.Csv.getRowsData (DI.Csv.readCsv excelDataMap)
        DI.Excel.matchColumName "Column 1" fromToPairs
        |> should equal "Column A"

    [<Test>]
    member x.``Return string "filterOut" if column not matched`` () =
        let fromToPairs = DI.Csv.getRowsData (DI.Csv.readCsv excelDataMap)
        DI.Excel.matchColumName "Column 3" fromToPairs
        |> should equal "filterOut"


    [<Test>]
    member x.``Change Column names`` () =
        let fromToPairs = DI.Csv.getRowsData (DI.Csv.readCsv excelDataMap)
        DI.Excel.changeColumnNames excelData fromToPairs
        |> Seq.item 0
        |> should equal "Column A"

    [<Test>]
    member x.``Changle only columns which are mapped and remove the rest`` () =
        let fromToPairs = DI.Csv.getRowsData (DI.Csv.readCsv excelDataMap)
                          |> Seq.filter (fun ftp -> 
                                          ftp.GetColumn "from" = "Column 1")
        DI.Excel.changeColumnNames excelData fromToPairs
        |> Seq.length
        |> should equal 1

    [<Test>]
    member x.``Change columns Names to Indexes`` () =
        let fromToPairs = DI.Csv.getRowsData (DI.Csv.readCsv excelDataMap)
                            |> Seq.map (fun row -> 
                                            row.GetColumn "from")
                            |> Seq.toArray

        DI.Excel.columnNamesToIndex excelData fromToPairs
        |> Array.length
        |> should equal 2

    [<Test>]
    member x.``Pick up only columns from the map file`` () =
        let fromToPairs = DI.Csv.getRowsData (DI.Csv.readCsv excelDataMap)
                            |> Seq.map (fun row -> 
                                            row.GetColumn "from")
                            |> Seq.filter (fun column ->
                                            column = "Column2")
                            |> Seq.toArray

        DI.Excel.columnNamesToIndex excelData fromToPairs
        |> Array.last
        |> should equal 1

    [<Test>]
    member x.``Change Indexes to Occurances`` () =
        let fromToPairs = DI.Csv.getRowsData (DI.Csv.readCsv excelDataMap)
                            |> Seq.map (fun row -> 
                                            row.GetColumn "from")
                            |> Seq.filter (fun column ->
                                            column = "Column2")
                            |> Seq.toArray

        DI.Excel.columnNamesToIndex excelData fromToPairs
        |> DI.Excel.columnNameIndexToOccurances
        |> Array.last
        |> should equal 2

    [<Test>]
    member x.``Eleventh element of array index is a every thid or fourth occurance`` () =
        DI.Excel.isNthColumn 11 [|3;1|] 4
        |> should equal true 

    [<Test>]
    member x.``Tenth element of array index is NOT an every third or fourth occurance`` () =
        DI.Excel.isNthColumn 10 [|0;1|] 4
        |> should equal false 

    [<Test>]
    member x.``Get exact column data of column 1`` () = 
        DI.Excel.getExactColumnsData excelData [|"Column 1"|]
        |> should equal ["A";"B"]

    [<Test>]
    member x.``Get exact column data of column2`` () = 
        DI.Excel.getExactColumnsData excelData [|"Column2"|]
        |> should equal [1.2;1.0]

    [<Test>]
    member x.``Parse the files and produce a map`` () =
        DI.Excel.parse excelPath null excelDataMap
        |> fst
        |> Seq.head
        |> should equal "Column A"
