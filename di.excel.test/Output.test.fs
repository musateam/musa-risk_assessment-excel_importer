namespace DI.Excel.Test

open NUnit.Framework
open FsUnit
open ExcelPackageF
open FSharp.Data

[<TestFixture>]
type ``Output formatting`` () =

    let excelPath = @"../tools/testData/testData.xlsx" 
    let excelData = DI.Excel.readExcel excelPath null
    let excelDataMap = @"../tools/testData/testDataMap.csv"

    [<Test>]
    member x.``Split sequence each 2 occurances`` () =
        DI.Output.splitSeqEach 2 [1;2;3;4;5;6;7;8;9;10]
        |> Seq.head
        |> Array.length
        |> should equal 2


    [<Test>]
    member x.``Wrap argument and value in quotes`` () =
        DI.Output.printArgumentValue "one" "two"
        |> should equal "\"one\": \"two\""

    [<Test>]
    member x.``Object to String if value is other than null`` () =
        DI.Output.objectToString "one"
        |> should equal "one"

    [<Test>]
    member x.``Object to String if value is null`` () =
        DI.Output.objectToString null
        |> should equal ""