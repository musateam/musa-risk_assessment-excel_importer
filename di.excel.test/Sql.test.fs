
namespace DI.Excel.Test 

open NUnit.Framework
open FsUnit

module Sql =

    [<Test>]
    let ``columns wrapped`` () =
        DI.Sql.Insert.columns "column1,column2" |> should equal "( column1,column2 )"

    [<Test>]
    let ``columns wrapped with quotes`` () =
        DI.Sql.Insert.columns "'column1', 'column2'" |> should equal "( 'column1', 'column2' )"

    [<Test>]
    let ``columns empty if not given`` () =
        DI.Sql.Insert.columns null |> should equal ""

    [<Test>]
    let ``insert into w/o columns`` () =
        DI.Sql.Insert.insertWhatWhere "tableName" null |> should equal "INSERT INTO tableName "

    [<Test>]
    let ``insert into with columns`` () =
        DI.Sql.Insert.insertWhatWhere "tableName" "column1,column2" |> should equal "INSERT INTO tableName ( column1,column2 )"

    [<Test>]
    let ``values wrapped`` () =
        DI.Sql.Insert.values "value1,value2" |> should equal " VALUES ( value1,value2 )"

    [<Test>]
    let ``values wrapped with quotes`` () =
        DI.Sql.Insert.values "'value1', 'value2'" |> should equal " VALUES ( 'value1', 'value2' )"

    [<Test>]
    let ``generate simple query`` () =
        DI.Sql.Insert.generate ("tableName", null, "'value1', 'value2'") |> should equal "INSERT INTO tableName  VALUES ( 'value1', 'value2' )"