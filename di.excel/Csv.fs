namespace DI

module Csv =

    open FSharp.Data
    let readCsv (filePath:string) =
        CsvFile.Load(filePath)

    let getColumnNames (csvData:CsvFile) =
        csvData.Headers.Value

    let getRowsData (csvData:CsvFile) =
        csvData.Rows

    let getRowsCount (csvData:CsvFile) =
        getRowsData csvData |> Seq.length

    let getExactColumnRow (csvData:CsvFile) (columnName:string) rowNumber =
        let row = csvData
                    |> getRowsData
                    |> Seq.item (rowNumber - 1)
        row.GetColumn columnName

    let getFromArr csvData =
        csvData
        |> getRowsData
        |> Seq.map (fun csvRow -> 
                        csvRow.GetColumn "from")
        |> Seq.toArray
