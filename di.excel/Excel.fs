namespace DI 

module Excel =

    open ExcelPackageF
    open FSharp.Data

    let readExcel filePath sheetName =
        match sheetName with
        | null -> filePath |> Excel.getWorksheetByIndex 1
        | _ -> filePath |> Excel.getWorksheetByName sheetName

    let getColumnNames excelData = 
        excelData 
        |> Excel.getRow 1

    let getColumnCount excelData =
        excelData
        |> Excel.getMaxColNumber

    let matchColumName columnName (fromToPairs:seq<CsvRow>) =
        let columnFound = fromToPairs
                          |> Seq.tryFind(fun fromTo -> fromTo.GetColumn "from" = columnName)
        match columnFound with
        | None -> "filterOut"
        | _ -> columnFound.Value.GetColumn "to"

    let changeColumnNames excelData (fromToPairs:seq<CsvRow>) = 
        excelData
        |> getColumnNames
        |> Seq.map (fun columnName -> matchColumName columnName fromToPairs)
        |> Seq.filter (fun columnName -> not (columnName.Equals("filterOut")))

    let getRowsData excelData =
        excelData 
        |> Excel.getContent 
        |> Seq.skip(excelData 
                    |> Excel.getMaxColNumber)

    let columnNamesToIndex excelData columnNames = 
        excelData
        |> getColumnNames
        |> Seq.mapi (fun index element -> index, element)
        |> Seq.filter (fun (i, el) ->
                            columnNames 
                            |> Array.exists (fun columnName -> 
                                                 columnName = el))
        |> Seq.map fst
        |> Seq.toArray

    let columnNameIndexToOccurances columnIndexes = 
        columnIndexes
        |> Array.map (fun i -> i + 1) 

    let isNthColumn columnIndex (nthColumns: int []) columnsCount = 
        nthColumns
        |> Array.map (fun el -> 
                        columnIndex % columnsCount = el)
        |> Array.contains true

    let getExactColumnsData excelData columnNames =
        excelData
        |> getRowsData
        |> Seq.mapi (fun index element -> element, index)
        |> Seq.filter (fun (el, i) -> 
                        let columnIndex = columnNamesToIndex excelData columnNames
                        let columnsCount = excelData 
                                           |> Excel.getMaxColNumber
                        isNthColumn i columnIndex columnsCount)
        |> Seq.map fst  


    let parse excelPath sheetName csvPath =
        let excelData = readExcel excelPath sheetName
        let csvData = Csv.readCsv csvPath
        let fromToPairs = csvData |> Csv.getRowsData 

        let columns = changeColumnNames excelData fromToPairs
        let data = getExactColumnsData excelData (csvData |> Csv.getFromArr)

        (columns, data)
