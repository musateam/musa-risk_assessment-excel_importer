namespace DI

module Output =

    open System

    let splitSeqEach each seq = 
        seq 
        |> Seq.chunkBySize each

    let wrapInQuotes (stringSeq:seq<string>) =
        stringSeq
        |> Seq.map (fun v ->
                        match v.[0] with
                        | '\'' | '"' -> v
                        | _ -> ["'";v;"'"]
                                |> String.concat "")

    let objectToString (v:obj) =
        match v with
        | value when Object.ReferenceEquals(value, null) -> ""
        | _ -> v.ToString()

    let printCsv (headers:seq<string>,data:seq<obj>,lineLimit:int) =
        printfn "%s" (headers |> String.concat ",")
        splitSeqEach lineLimit data
        |> Seq.iter (fun row -> 
                        printfn "%s" (row 
                                      |> Array.map(fun v -> v |> objectToString)
                                      |> String.concat ","))

    
    let printArgumentValue argument value = 
        [
            "\"";
            argument;
            "\": \"";
            value;
            "\""
        ] |> String.concat ""

    let printJson (headers:seq<string>,data:seq<obj>,lineLimit:int) =
        let headers = headers |> Seq.toList
        let dataString = splitSeqEach lineLimit data
                         |> Seq.map(fun row ->
                                        let objectAsString = row 
                                                             |> Array.mapi (fun index v ->
                                                                                let v = v |> objectToString
                                                                                printArgumentValue headers.[index] v)
                                                             |> String.concat ","
                                                                                
                                        [
                                            "{";
                                            objectAsString
                                            "}"
                                        ] |> String.concat "")
                         |> String.concat ","

        let wrappedDataString =
            [
                "[";
                dataString 
                "]"
            ] |> String.concat ""

        printfn "%s" wrappedDataString

    let printSql (headers:seq<string>,data:seq<obj>,lineLimit,tableName) =
        splitSeqEach lineLimit data
        |> Seq.iter(fun row ->
                        printfn "%s" <| Sql.Insert.generate (
                            tableName,
                            headers |> wrapInQuotes |> String.concat ",",
                            row |> Array.map(fun v -> 
                                                 [
                                                     "'";
                                                     v |> objectToString;
                                                     "'"
                                                 ] |> String.concat "") 
                                |> String.concat ","))

    let print (headers:seq<string>,data:seq<obj>,lineLimit:int, format, tableName) =
        match format with
        | "csv" -> printCsv (headers,data,lineLimit)
        | "sql" -> printSql (headers,data,lineLimit,tableName)
        | _ -> printJson (headers,data,lineLimit)
        