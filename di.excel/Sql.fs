namespace DI.Sql

module Insert =

    let columns columnString =
        match columnString with 
        | null -> ""
        | _ ->  ["( "; columnString; " )"] |> String.concat ""

    let insertWhatWhere tableName columnsString =
        ["INSERT INTO"; tableName; columns columnsString] |> String.concat " "

    let values valuesString =
        [" VALUES ("; valuesString; ")"] |> String.concat " "

    let generate (tableName, columnsString, valuesString) =
        [
            insertWhatWhere tableName columnsString;
            values valuesString
        ] |> String.concat ""