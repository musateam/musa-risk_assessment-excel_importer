namespace DI

module main = 

    open Argu

    type Arguments =
        | [<Mandatory>][<Unique>] Excel_File of path:string
        | Excel_Worksheet of sheet:string
        | [<Mandatory>][<Unique>] Map_File of path:string
        | Output_Type of format:string
        | Table_Name of name:string
    with
        interface IArgParserTemplate with
            member s.Usage = 
                match s with
                | Excel_File _ -> "specify source Excel file (xls|xlsx)"
                | Excel_Worksheet _ -> "specify a worksheet name to process, if none given, the first one worksheet of the file is taken"
                | Map_File _ -> "specify transformation map file in CSV format"
                | Output_Type _ -> "specify output type (default: Json) <json|csv|sql>"
                | Table_Name _ -> "specify final database tableName"


    [<EntryPoint>]
    let main argv =
        let parser = ArgumentParser.Create<Arguments>(programName = "musa-excel-import.exe", errorHandler = ProcessExiter())
        let argvResults = parser.ParseCommandLine argv

        let data = Excel.parse 
                        (argvResults.GetResult <@ Excel_File @>) 
                        (argvResults.GetResult(<@ Excel_Worksheet @>, defaultValue = null))
                        (argvResults.GetResult <@ Map_File @>)

        Output.print
                    ((data |> fst),
                    (data |> snd),
                    (Csv.readCsv (argvResults.GetResult <@ Map_File @>) |> Csv.getRowsCount),
                    (argvResults.GetResult (<@ Output_Type @>, defaultValue = "json")),
                    (argvResults.GetResult (<@ Table_Name @>, defaultValue = "")))

        0 // return an integer exit code
